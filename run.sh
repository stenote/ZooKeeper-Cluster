#!/bin/bash

docker network create --subnet 172.18.0.1/16 zookeeper

docker run -d --hostname zookeeper-server-0 --ip 172.18.0.2 --network zookeeper -p 2181:2181  --name zookeeper-server-0 -v "`pwd`/server.0/":/conf -v "`pwd`/server.0/":/data zookeeper:latest
docker run -d --hostname zookeeper-server-1 --ip 172.18.0.3 --network zookeeper -p 3181:2181 --name zookeeper-server-1 -v "`pwd`/server.1/":/conf -v "`pwd`/server.1/":/data zookeeper:latest
docker run -d --hostname zookeeper-server-2 --ip 172.18.0.4 --network zookeeper -p 4181:2181 --name zookeeper-server-2 -v "`pwd`/server.2/":/conf -v "`pwd`/server.2/":/data zookeeper:latest
