
## 集群部署

在 ZooKeeper 做集群的时候，需要先根据需要，创建集群的配置信息，如下：

```
tickTime=2000
# 心跳时间

initLimit=5
# Follower / Leader 初始化链接最多心跳次数

syncLimit=2
# Follower / Leader 通讯最多能允许的心跳次数

clientPort=3181
# 客户端口

# dataDir=/Users/may/z1/
数据目录
#########
# 后续为 server.0 / server.1 / server.2 集群的配置信息
server.0=127.0.0.1:2888:3888
server.1=127.0.0.1:4888:5888
server.2=127.0.0.1:6888:7888
# server 配置信息分别为 服务器地址:通讯端口:选举端口
```

存储如上配置文件为 z1.cfg 后，继续创建 z2.cfg ，z3.cfg，需要注意，修改其中的 dataDir 和 clientPort (如果在同一台机器上）

然后分别在对应的 dataDir 中，创建 myid 文件，内容写入自己的 id

比如 server.0 就需要创建 /Users/may/z0/myid ，写入 0
server.1 就需要创建 /Users/may/z1/myid ，写入 1

创建完成后启动 ZooKeeper 集群。

```
zkServer start path/to/z1.cfg
zkServer start path/to/z2.cfg
zkServer start path/to/z3.cfg
```

run.sh 脚本中是使用了 server.0 / server.1 / server.3 来分别表示三个 ZooKeeper 实例


## standalone 模式

最小配置文件

```
tickTime=2000
clienPort=3181
dataDir=/Users/may/z1/
```

不用配置 `syncLimit` / `initLimit` / `server.x` 等信息, 直接启动即可

```
zkServer start path/to/standalone.cfg
```


